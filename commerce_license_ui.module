<?php

/**
 * @file
 */

/**
 * Implements hook_menu().
 */
function commerce_license_ui_menu() {
  $items = array();

  // Note: admin/commerce/licenses is defined by a default View.

  // @todo Modify the main view with links to the "add" buttons.
  // @todo Add a views handler for edit links.

  $items['admin/commerce/licenses/%entity_object/edit'] = array(
    'load arguments' => array('commerce_license'),
    'title' => 'Edit a Commerce License',
    'description' => 'Edit an existing commerce license.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('commerce_license_ui_license_form', 3),
    'access arguments' => array('administer licenses'),
  );

  foreach (commerce_license_get_type_plugins() as $type => $license_plugin) {
    $items['admin/commerce/licenses/add/' . strtr($type, array('_' => '-'))] = array(
      'title' => 'Create @name Commerce License',
      'title arguments' => array('@name' => $license_plugin['title']),
      'description' => 'Add a new commerce license manually.',
      'page callback' => 'drupal_get_form',
      'page arguments' => array(
        'commerce_license_ui_license_form',
        entity_create('commerce_license', array('type' => $type)),
      ),
      'access arguments' => array('administer licenses'),
    );
  }

  return $items;
}

/**
 * Form for manual license creation or editing.
 */
function commerce_license_ui_license_form($form, &$form_state, $license) {
  // We don't use the "entity form" because license uses IEF for
  // configurable licenses. The fields should still be fetched and
  // show here though.
  $form_state['license'] = $license;

  if ($license->uid) {
    $account = user_load($license->uid);
  }

  $form['username'] = array(
    '#type' => 'textfield',
    '#title' => t('Username'),
    '#description' => t('User who should receive the license.'),
    '#maxlength' => 60,
    '#autocomplete_path' => 'user/autocomplete',
    '#default_value' => isset($account) ? $account->name : '',
    '#required' => TRUE,
    '#weight' => -1,
  );

  // @todo Licence types could be using a smaller subset of this
  // list. There should be a lookup function based on the object.
  $product_types = commerce_license_product_types();
  $products = commerce_product_load_multiple(array(), array('type' => $product_types));
  $product_options = array();
  foreach ($products as $product) {
    $product_options[$product->product_id] = $product->title;
  }

  $form['product'] = array(
    '#type' => 'select',
    '#title' => t('Product'),
    '#description' => t('Only license product types are available.'),
    '#options' => $product_options,
    '#default_value' => $license->product_id,
    '#required' => TRUE,
  );

  $form['granted'] = array(
    '#type' => 'date_popup',
    '#title' => t('Granted'),
    '#description' => t('Date that this license was granted. You will likely not want to touch this unless you are manually correcting some historical records.'),
    '#date_label_position' => FALSE,
    '#default_value' => $license->granted ? date('Y-m-d', $license->granted) : date('Y-m-d'),
    '#required' => TRUE,
  );

  $form['expires'] = array(
    '#type' => 'date_popup',
    '#title' => t('Expires'),
    '#description' => t('Set a custom expiry date independent of the selected product. You can increase or decrease an existing license, but you should use the <em>renew</em> action instead if you are increasing.'),
    '#date_label_position' => FALSE,
    '#default_value' => $license->expires ? date('Y-m-d', $license->expires) : date('Y-m-d', strtotime('+1 year')),
    '#required' => TRUE,
  );

  $form['status'] = array(
    '#type' => 'select',
    '#title' => t('Status'),
    '#description' => t('Disabled profiles will not be visible to customers in options lists.'),
    '#options' => commerce_license_status_options_list(),
    '#default_value' => $license->status,
    '#required' => TRUE,
  );

  // We add the form's #submit array to this button along with the actual submit
  // handler to preserve any submit handlers added by a form callback_wrapper.
  $submit = array();

  if (!empty($form['#submit'])) {
    $submit += $form['#submit'];
  }

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save license'),
    '#submit' => array_merge($submit, array('commerce_license_ui_license_form_submit')),
  );

  // We append the validate handler to #validate in case a form callback_wrapper
  // is used to add validate handlers earlier.
  $form['#validate'][] = 'commerce_license_ui_license_form_validate';

  // Add fields from license type.
  field_attach_form('commerce_license', $license, $form, $form_state, LANGUAGE_NONE);

  // The num_renewals field should not be editable by the customer.
  $form['num_renewals']['#access'] = FALSE;

  return $form;
}

/**
 * Validation handler.
 */
function commerce_license_ui_license_form_validate($form, &$form_state) {
  // @todo We do need to verify if the user account exists. But,
  // licenses can be anonymous, so maybe make that an option.
}

/**
 * Submit handler.
 */
function commerce_license_ui_license_form_submit($form, &$form_state) {
  $license = $form_state['license'];
  $values = $form_state['values'];

  $license->uid = user_load_by_name($values['username'])->uid;
  $license->product_id = $values['product'];
  $license->expires = strtotime($values['expires']);
  $license->granted = strtotime($values['granted']);
  $license->status = $values['status'];

  $license->save();

  // @todo Fields provided by "attach form" need to be saved too.

  // Redirect user and inform them of success.
  $form_state['redirect'] = 'admin/commerce/licenses';
  $args = array('@id' => $license->license_id, '@user' => $values['username']);
  drupal_set_message(t('License #@id for @user has been saved.', $args));
}
